package example;
import com.opensymphony.xwork2.ActionContext;
import java.util.Map;

public class Logout {

    
    public String execute() throws ClassNotFoundException {
        Map session = ActionContext.getContext().getSession();
        session.clear();
        return "success";
    }
}
