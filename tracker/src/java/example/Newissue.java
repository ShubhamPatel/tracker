package example;
import com.opensymphony.xwork2.ActionContext;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class Newissue {

    private String title, body;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String execute() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection(DbConfig.URL,DbConfig.username,DbConfig.password);
        Statement stmt = con.createStatement();
        Date d = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("h:mm a 'at' dd/MM/yyyy");
        Map ses = ActionContext.getContext().getSession();
        String name=(String) ses.get("name");
        String username=(String) ses.get("username");   
        String time=ft.format(d);
        String query = "INSERT INTO issues values(null,'" + name + "' ,'" + username + "','" + title + "','" + body + "','" + time + "')";
        stmt.execute(query);
        con.close();
        return "success";
    }   
}
