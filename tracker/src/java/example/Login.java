package example;

import com.opensymphony.xwork2.ActionContext;
import java.sql.*;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

public class Login {

    private String username, userpass;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass;
    }

    public String execute() throws ClassNotFoundException {
        Boolean login = null;
        Connection con = null;
        String dbPassword, dbUsername,dbName=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(DbConfig.URL, DbConfig.username, DbConfig.password);

            Statement stmt = con.createStatement();
            String query = "SELECT * FROM login";
            stmt.executeQuery(query);
            ResultSet rs = stmt.getResultSet();
            System.out.println("yo");
            while (rs.next()) {
                dbUsername = rs.getString("username");
                dbPassword = rs.getString("password");
                dbName = rs.getString("name");
                if (username.equals(dbUsername) && userpass.equals(dbPassword)) {
                    login = true;
                } else {
                    login = false;
                }
            }
        } catch (SQLException | ClassNotFoundException e) {
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                }
            }
        }

        if (login == true) {
            Map session = ActionContext.getContext().getSession();
            session.put("username", username);
            session.put("name", dbName);
            return "success";
        } else {
            return "error";
        }
    }
}
