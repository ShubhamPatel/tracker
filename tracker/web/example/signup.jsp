<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Issue Tracker</title>
        <link href="${pageContext.request.contextPath}/example/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h1>Issue Tracker</h1>
        <s:form cssClass="form" action="signup">  
            <s:textfield  name="name" placeholder="Enter your Name"></s:textfield>  
            <s:textfield  name="username" placeholder="Enter Username"></s:textfield>  
            <s:password  name="userpass" placeholder="Enter Password"></s:password>  
            <s:submit value="Sign Up"></s:submit>  
        </s:form>
        <s:a href="/tracker/example/index.jsp">Login Here</s:a>
    </body>
</html>

