<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Map"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
    Map ses = ActionContext.getContext().getSession();
    if (!ses.containsKey("name")) {
        response.sendRedirect("/Issue Tracker/example/index.jsp");
    }
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Issue Tracker</title>
        <link href="${pageContext.request.contextPath}/example/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h3 style="text-align:left; margin-left: 20px; float:left;">Welcome, <% out.print((String) ses.get("name")); %></h3>
        <h3 style="text-align:right; margin-right: 20px; float:right;"><s:form action="logout">   
                <s:submit value="Logout"></s:submit>  
            </s:form> </h3>
        <br>
        <br>
        <h1 style="text-align:center;">Issue Tracker</h1>
        <s:form cssClass="form" action="newissue">  
            <s:textfield  name="title" placeholder="Enter Issue Title"></s:textfield>  
            <s:textarea rows="4" name="body" cssClass="textarea" placeholder="Enter Issue"></s:textarea>  
            <s:submit value="Submit Issue"></s:submit>
        </s:form>
        <h1 style="text-align:center;">Current Issues</h1>
        <table id="issues">
            <tr>
                <th>Issue Title</th>
                <th>Issue</th>
                <th>Created By</th>
                <th>Creation Time</th>
            </tr>
            <%
                Connection con = null;
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                    con = DriverManager.getConnection("jdbc:mysql://localhost/tracker", "root", "");

                    Statement stmt = con.createStatement();
                    String query = "SELECT * FROM issues ORDER BY Sr_no DESC";
                    stmt.executeQuery(query);
                    ResultSet rs = stmt.getResultSet();

                    while (rs.next()) {
                        out.println("<tr><td>" + rs.getString("title") + "</td><td>" + rs.getString("body") + "</td><td>" + rs.getString("name") + "</td><td>" + rs.getString("time") + "</td></tr>");
                    }
                } catch (SQLException e) {

                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException e) {
                        }
                    }
                }

            %>
        </table>
    </body>
</html>

