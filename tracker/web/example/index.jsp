<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="${pageContext.request.contextPath}/example/style.css" rel="stylesheet" type="text/css"/>
        <title>Issue Tracker</title>
    </head>
    <body>
        <h1>Issue Tracker</h1>
        <s:form cssClass="form" action="login">  
            <s:textfield  name="username" placeholder="Enter Username"></s:textfield>  
            <s:password  name="userpass" placeholder="Enter Password"></s:password>  
            <s:submit value="Login"></s:submit>  
        </s:form>
        <s:a href="/tracker/example/signup.jsp">Sign Up Here</s:a>
    </body>
</html>

