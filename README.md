# Issue Tracking System #

## What is this repository for? ##

Users can login/signup and submit their issues and view issues submitted by others.

## Technologies/Library used ##

* Java Struts 2
* MySQL
* Struts 2 support plugin for Netbeans.
Link - [https://sourceforge.net/projects/struts2nbplugin/](Link URL)

### Requirements ###

* Netbeans IDE v 8.0 or higher.
* Apache HTTP server
* Apache Tomcat v 7.0 or higher.
* MySQL database

### How do I get set up? ###

1.
Download Repository as zip file.
Open Netbeans and Goto File->Import->From Zip.
Select downloaded zip file.

2.
Start MySQL server.
Open phpMyAdmin and Goto Import->Browse.
Select tracker.sql file from the repository.

3.
Edit DbConfig.java file according to your settings.
Run "tracker" from Netbeans IDE.

After running the code, You will be redirected to Login Page.Click "Sign up Here".
Enter your details and Sign up.

### Database "Tracker"###

* login - login table user login details such as Name,Username and Password.
* issues - issues contains information about issues submitted by users.

### Files ###

JSP Files

* /web/example/index.jsp - Contains Login Page.
* /web/example/signup.jsp - Contains Signup Page.
* /web/example/loginfailed.jsp - Contains Failure Page for login.
* /web/example/signupdone.jsp - To inform user that registeraion is complete.
* /web/example/issue.jsp - To Submit/View Issu After Login

Java Files

* /src/java/example/DbConfig.java - Configuartion file for Database Access
* /src/java/example/Login.java - To Autorize login credentials
* /src/java/example/Logout.java - To Delete Session and Log out User
* /src/java/example/Register.java - To Register a new User in Database
* /src/java/example/NewIssue.java - To Enter New Issue in Database

CSS Files

* /web/example/style.css - Contains Styles for Views

XML File

* /src/java/struts.xml - To Map Views with Actions.
* /web/WEB-INF/web.xml - Web Application Deployment Descriptor of Application